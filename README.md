# OBSOLETED
## No Longer Maintanined Here
Moved to [Device-Mirror-Plus](https://github.com/terrelsa13/Device-Mirror-Plus)

~~# Device Mirror Plus~~

~~One master [Switch], [Motion Sensor], [Contact Sensor], [Humidity Sensor], [Temperature Sensor], [Battery Level], [Audio] or [Single Button] to control one or more slaves.~~

- ~~Motion Sensor can now mirror - Motion, Temperature, Illuminance, Humidity, Acceleration, Tamper, and Battery~~
- ~~Contact Sensor can now mirror - Contact, Temperature, Tamper, and Battery~~
- ~~Humidity Sensor can mirror - Humidity, Temperature, and Battery~~
- ~~Temperature Sensor can mirror - Temperature, Humidity, and Battery~~
- ~~Battery Only can mirror - Battery~~

